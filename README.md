Role Name
=========

A role that installs the Ariadne+ Ephemera Docker stack.

Role Variables
--------------

The most important variables are listed below:

``` yaml
ephemera_compose_dir: '/srv/ephemera_stack'
ephemera_docker_stack_name: 'ephemera-ariadne'
ephemera_compose_file_prefix: ephemera-docker-compose
ephemera_remote_compose_file: false
ephemera_compose_url: 'http://localhost/{{ ephemera_compose_file_prefix }}-stack.yml.j2'
# ephemera_mysql_root_password: 'set it in a vault file'
# ephemera_mysql_password: 'set it in a vault file'
ephemera_nfs_server: 127.0.0.1
```

Dependencies
------------

A docker swarm cluster is required

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
